<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CadastroController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::post('/cadastrar-link', [CadastroController::class, 'cadastrarLink'])->name('cadastrar-link');
Route::post('/cadastrar-nota', [CadastroController::class, 'cadastrarNota'])->name('cadastrar-nota');

Route::get('/deletar-link/{id}', [CadastroController::class, 'deletarLink'])->name('deletar-link');
Route::get('/deletar-nota/{id}', [CadastroController::class, 'deletarNota'])->name('deletar-nota');
