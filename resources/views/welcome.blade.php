<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="http://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">


</head>
<style>
    body.modal-open {
        overflow: hidden;
    }
</style>
<body>
    <nav class="nav-extended">
        <div class="nav-wrapper">
            <a href="#" class="brand-logo"></a>
            <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li><a href="#">Alterar papel de parede</a></li>
            </ul>
        </div>
    </nav>

    <div class="row" style="margin-top: 30px;">
        <!-- -->
        <div class="col s4"></div>

        <!-- links -->
        <div class="col s4">
            <a class="waves-effect waves-light light-blue darken-3 btn modal-trigger" href="#modal2">Adicionar Link</a>

            <!-- Modal Structure -->
            <div id="modal2" style="height: 35%" class="modal">
                <form class="col s12" action="{{ route('cadastrar-link') }}" method="POST">
                    @csrf
                    <div class="modal-content">
                        <h4>Novo Link</h4>
                        <div class="row">
                            <div class="input-field col s6">
                                <input placeholder="Site Delcca" id="texto" name="texto" type="text" class="validate" required autofocus>
                                <label for="texto">Texto</label>
                            </div>
                            <div class="input-field col s6">
                                <input placeholder="https://delcca.com.br" id="" name="url" type="text" class="validate" required autofocus>
                                <label for="first_name">URL</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="waves-effect waves-green btn-flat">Cadastrar</button>
                    </div>
                </form>
            </div>

            <div class="row" style="padding-top: 30px;">
                <div class="col" >
                    @foreach(\Illuminate\Support\Facades\DB::table('link')->get() as $link)
                        <a href="{{ $link->url }}" style="margin-right: 5px; margin-top: 5px;" class="waves-effect waves-light btn-large">{{$link->texto}}</a>
                    @endforeach
                </div>

            </div>





        </div>

        <!-- to do list -->
        <div class="col s4">

            <!-- Modal Trigger -->
            <a class="waves-effect waves-light light-blue darken-3 btn modal-trigger" href="#modal3">Adicionar anotação</a>

            <!-- Modal Structure -->
            <div id="modal3" style="height: 35%" class="modal">
                <div class="modal-content">
                    <h4>Nova Anotação</h4>
                    <form class="col s12" action="{{ route('cadastrar-nota') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="Descreva sua nota" id="" name="texto" type="text" class="validate" autofocus>
                                <label for="first_name">Anotação</label>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="waves-effect waves-green btn-flat">Cadastrar</button>
                        </div>
                    </form>
                </div>
            </div>

            <table>
                <thead>
                <tr>
                    <th>Nota</th>
                    <th>Options</th>
                </tr>
                </thead>

                <tbody>
                    @foreach(\Illuminate\Support\Facades\DB::table('note')->get() as $note)
                        <tr>
                            <td>{{ $note->texto }}</td>
                            <td> <a href="{{ route('deletar-nota', ["id" => $note->id ]) }}" class="waves-effect waves-light btn-small red"><i class="material-icons">delete</i></a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>



<script
    src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<script src="http://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>
    {!! Toastr::message() !!}
<script>
    $(document).ready(function(){
        $('.modal').modal();
    });
</script>
</body>
</html>
