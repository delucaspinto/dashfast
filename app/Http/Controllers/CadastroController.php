<?php

namespace App\Http\Controllers;

use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CadastroController extends Controller
{
    public function cadastrarLink(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'texto' => 'required',
            'url' => 'required'
        ]);

        if ($validator->fails()) {

            Toastr::error("Preencha o formulário corretamente!", '', [
                "closeButton" => false,
                "debug"=> false,
                "newestOnTop"=> true,
                "progressBar"=> true,
                "positionClass"=> "toast-top-left",
                "preventDuplicates"=> true,
                "onclick"=> null,
                "showDuration"=> "400",
                "hideDuration"=> "1000",
                "timeOut"=> "5000",
                "extendedTimeOut"=> "1000",
            ]);

            return redirect()->back();
        }

        DB::table('link')->insert([
            "texto" => $request->texto,
            "url" => $request->url,
            "user_id" => 1
        ]);

        return redirect()->route('home');

    }

    public function cadastrarNota(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'texto' => 'required',
        ]);

        if ($validator->fails()) {

            Toastr::error("Preencha o formulário corretamente!", '', [
                "closeButton" => false,
                "debug"=> false,
                "newestOnTop"=> true,
                "progressBar"=> true,
                "positionClass"=> "toast-top-left",
                "preventDuplicates"=> true,
                "onclick"=> null,
                "showDuration"=> "400",
                "hideDuration"=> "1000",
                "timeOut"=> "5000",
                "extendedTimeOut"=> "1000",
            ]);

            return redirect()->back();
        }

        DB::table('note')->insert([
            "texto" => $request->texto,
            "user_id" => 1
        ]);

        return redirect()->route('home');

    }

    public function deletarNota($id)
    {
        DB::table('note')->delete($id);

        Toastr::success("Removido com sucesso!", '', [
            "closeButton" => false,
            "debug"=> false,
            "newestOnTop"=> true,
            "progressBar"=> true,
            "positionClass"=> "toast-top-left",
            "preventDuplicates"=> true,
            "onclick"=> null,
            "showDuration"=> "400",
            "hideDuration"=> "1000",
            "timeOut"=> "5000",
            "extendedTimeOut"=> "1000",
        ]);

        return redirect()->route('home');
    }

    public function deletarLink($id)
    {

        DB::table('link')->delete($id);

        return redirect()->route('home');
    }
}
